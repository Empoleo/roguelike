﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Menu : MonoBehaviour {
    public Canvas MainCanvas;
    public Canvas HUD;
    public Canvas Info;


	void Start () {
        MainCanvas.enabled = false;
        HUD.enabled = false;
        Info.enabled = false;
        bool b = GameController.Instance.NeedsMenu();
        bool m = GameController.Instance.NeedsHUD();
        MainCanvas.enabled = b;
        HUD.enabled = m;
    }
	
	// Update is called once per frame
	void Update () {
	
	}

    public void Begin()
    {
        MainCanvas.enabled = false;
        HUD.enabled = true;
        Info.enabled = false;
    }
    public void HowTo()
    {
        MainCanvas.enabled = false;
        HUD.enabled = false;
        Info.enabled = true;
    }
    public void Return()
    {
        MainCanvas.enabled = true;
        HUD.enabled = false;
        Info.enabled = false;
    }

}

