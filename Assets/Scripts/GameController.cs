﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class GameController : MonoBehaviour {

    public static GameController Instance;
    public bool isPlayerTurn;
    public bool areEnemiesMoving;
    public int playerCurrentHealth = 50;
    public AudioClip gameOverSound;
    public Canvas MainCanvas;
    public Text LevelText;

    private BoardController boardController;
    private List<Enemy> enemies;
    private GameObject levelImage;
    private Text levelText;
    private bool settingUpGame;
    private bool needsMenu = true;
    private bool needsHUD = false;
    private bool needsStartsScreen = true;
    private int secondsUntilLevelStart = 2;
    private int currentLevel = 1;

	void Awake () {
        if (Instance != null && Instance != this)
        {
            DestroyImmediate(gameObject);
            return;
        }
        Instance = this;
        DontDestroyOnLoad(gameObject);
        boardController = GetComponent<BoardController>();
        enemies = new List<Enemy>();
	}

    void Pregame()
    {

    }

    void Start()
    {
        InitializeGame();
    }

    private void InitializeGame()
    {
        settingUpGame = true;
        levelImage = GameObject.Find("Level Image");
        levelText = GameObject.Find("Level Text").GetComponent<Text>();
        if (needsStartsScreen)
        {
            needsStartsScreen = false;
            secondsUntilLevelStart = 0;
        }
        else
        {
            needsHUD = true;
            levelText.text = "Day " + currentLevel;
            secondsUntilLevelStart = 2;
        }
        levelImage.SetActive(true);
        enemies.Clear();
        boardController.SetupLevel(currentLevel);
        Invoke("DisableLevelImage", secondsUntilLevelStart);
    }

    private void DisableLevelImage()
    {
        levelImage.SetActive(false);
        settingUpGame = false;
        isPlayerTurn = true;
        areEnemiesMoving = false;
    }

    private void OnLevelWasLoaded(int leveelLoaded)
    {
        currentLevel++;
        InitializeGame();
    }
	
	void Update () {
        if(isPlayerTurn || areEnemiesMoving || settingUpGame)
        {
            return;
        }
        StartCoroutine(MoveEnemies());
	}

    private IEnumerator MoveEnemies()
    {
        areEnemiesMoving = true;

        yield return new WaitForSeconds(0.2f);

        foreach(Enemy enemy in enemies)
        {
            enemy.MoveEnemy();
            yield return new WaitForSeconds(enemy.moveTime);
        }

        areEnemiesMoving = false;
        isPlayerTurn = true;
    }

    public void AddEnemyToList(Enemy enemy)
    {
        enemies.Add(enemy);
    }

    public void GameOver()
    {
        isPlayerTurn = false;
        SoundController.Instance.music.Stop();
        SoundController.Instance.PlaySingle(gameOverSound);
        levelText.text = "You starved after " + currentLevel + " days...";
        levelImage.SetActive(true);
        enabled = false;
    }
    public bool NeedsMenu()
    {
        if (needsMenu)
        {
            needsMenu = false;
            return true;
        }
        return false;
    }
    public bool NeedsHUD()
    {
        if (needsHUD)
        {
            needsMenu = false;
            return true;
        }
        return false;
    }
}
